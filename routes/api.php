<?php

use App\Http\Controllers\InvitationController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/csrf-token', function () {
    return response()->json(['token' => csrf_token()]);
});
Route::middleware('auth:api')->group(function () {
    Route::get('/user', function (Request $request) {
        $user = Auth::user();

        // Customize the user info data as needed
        $userInfo = [
            'email' => $user->email,
            'name' => $user->name
            // Add any other user info you want to include
        ];

        // Return the user info as JSON
        return response()->json($userInfo);
    });
    Route::resource('invitation', InvitationController::class);
});