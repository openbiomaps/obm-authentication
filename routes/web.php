<?php

use App\Http\Controllers\InvitationController;
use App\Http\Controllers\UserInfoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::view('/', 'welcome');

Route::view('dashboard', 'dashboard')
    ->middleware(['auth', 'verified'])
    ->name('dashboard');

Route::view('profile', 'profile')
    ->middleware(['auth'])
    ->name('profile');

Route::view('invitation', 'invitation')->middleware(['auth'])->name('invitation');

Route::get('/userinfo', [UserInfoController::class, 'getUserInfo'])->middleware(['auth'])->name('userinfo');

Route::get('.well-known/openid-configuration', function () {
    
        return response()->json([
            'issuer' => 'https://auth.local',
            'authorization_endpoint' => 'https://auth.local/oauth/authorize',
            'token_endpoint' => 'http://auth.local/oauth/token',
            'userinfo_endpoint' => 'http://auth.local/api/user',
            'jwks_uri' => 'http://auth.local/.well-known/jwks'
        ]);
    });

Route::get('.well-known/jwks', function () {
    return response()->json([
        "keys" => [
            [
            "kty" => "RSA",
            "n" => "yCqojB4h7vlL1lE41R2003QBpPXDRCkIL3Ia4Xba8HTnw5W6GlX23cIu92En_TWtW0RwUc6VBWoW4523k9DqUEerDEGKe9h5ivzTogv-wOxSxToAB0yU4ZetP0kWIAvTNspPwvH-n-oLvQTPTgt5MCV0aNOC97dI52-5ctUykdoopKoS0Em6ACPzwHPFyX-NhCcLOe-tMbpvQvCWopxjUPvlLVCt5QZOJPyFdJvpltxFqpuKflZ98QLi9_I6RUN4Bty0YF7ndx82tH5OLe0CvqJ3uwWptMoV6uHYA2zhiGhCS3Rm1UjR-Vmm88yRO7m5ubNzEeEaGlK-RMySAcnf3WnjSVTOba7w56RqFgNuitHRXdM0ATyoozEZ0iYsFXe5et25P57ny9HuE5eHFlAEMl6Piv0BZ63Q9oIMNx-8_iRYdwjCzMgD5DCE5jk34Zj1FTh9YN-5Obq2olEbqpzxWQQC4hb0zNKPVWwhIUwTDvsTj5u7Bhp-cMX3XmeOZfKVUKzxvGk9kJ0yLO2JUS242cN_NqPUpZqKO3oYcilg2ZO8LCBArStB8da99OFR4FyYv-4yUFTbKeWbIIYBnkmFbRNA7k-W6qCnCi1kPkiPA2_BfVJk42U9cvwetGw6Wp-WD6786JHWOjVrwxpI_Zjb1WxbXioxJcDHZEjsspfvzGc",
            "e" => "AQAB",
            "alg" => "RS256",
            "use" => "sig",
            'kid' => base64_encode('public-key-added-2023-01-01')
            ]
        ]
    ]);
});

require __DIR__.'/auth.php';
