<div>
    @isset ( $viewLink )
        <a href="{{ $viewLink }}"><i class="fa-solid fa-eye me-2"></i></a>
    @endif
 
    @isset ( $editLink )
        <a href="{{ $editLink }}"><i class="fa-solid fa-pen-to-square me-2"></i></a>
    @endif
 
    @isset ( $deleteLink )
        <form
            action="{{ $deleteLink }}"
            class="d-inline"
            method="POST"
            x-data
            @submit.prevent="if (confirm('Are you sure you want to delete this user?')) $el.submit()"
        >
            @method('DELETE')
            @csrf
            <x-danger-button type="submit" class="btn btn-link">
                DELETE
            </x-danger-button>
        </form>
    @endif

    @isset ( $customLink )
        <form
            action="{{ $customLink['location'] }}"
            class="d-inline"
            method="{{ $customLink['method'] }}"
            x-data
            @submit.prevent="if (confirm('{{ $customLink['confirmMessage'] }}')) $el.submit()"
        >
            @csrf
            <x-danger-button type="submit" class="btn btn-link">
                {{ $customLink['label'] }}
            </x-danger-button>
        </form>  
    @endif
</div>