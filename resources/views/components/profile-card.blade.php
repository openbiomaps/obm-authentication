<div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
    <section>
        <header>
            <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
                {{ $title }}
            </h2>

            <p class="mt-1 mb-3 text-sm text-gray-600 dark:text-gray-400">
                {{ $subtitle }}
            </p>
        </header>
    </section>
    {{ $slot }}
</div>