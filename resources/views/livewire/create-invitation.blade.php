<form wire:submit="save" class="mt-6 space-y-6">
    <div>
        <x-input-label for="name" :value="__('Name')" />
        <x-text-input wire:model="name" id="name" name="name" type="text" class="mt-1 block w-full" required autofocus autocomplete="name" />
        <x-input-error class="mt-2" :messages="$errors->get('name')" />
    </div>
    <div>
        <x-input-label for="email" :value="__('Email')" />
        <x-text-input wire:model="email" id="email" name="email" type="email" class="mt-1 block w-full" required autocomplete="email" />
        <x-input-error class="mt-2" :messages="$errors->get('email')" />
    </div>
    <div>
        <x-input-label for="message" :value="__('Message')" />
        <x-text-input wire:model="message" id="message" name="message" type="text" class="mt-1 block w-full" required autocomplete="message"></x-text-input>
        <x-input-error class="mt-2" :messages="$errors->get('message')" />
    </div>
    <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Save') }}</x-primary-button>

            <x-action-message class="me-3" on="invitation-sent">
                {{ __('Invitation sent.') }}
            </x-action-message>
        </div>
</form>