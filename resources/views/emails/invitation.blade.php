<h2>Dear {{ $invitation->name }}</h2>

<p>{{ $invitation->message }}</p>
<br>
<p>{{ $inviter }} invites you to use a database that was created in the OpenBioMaps Data Management System.</p>
<p>In the OpenBioMaps framework, you get access to the following project:</p>
<br>
<p>
<em>%PROJECT_TITLE%</em>
</p>
<p>%PROJECT_DESCRIPTION%</p>
<br>
<p><strong>To accept the invitation follow this link:</strong> <a href="%PROTOCOL%://%URL%/index.php?registration=%code%">%PROTOCOL%://%URL%/index.php?registration=%code%</a></p>
<p>This invitation link works only once!</p>
<br>
<p>Best wishes,</p>
<p>{{ $inviter }} (%DOMAIN%)</p>
