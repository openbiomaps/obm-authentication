<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Invitations') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <x-profile-card title="Send invitation" subtitle="Send an invitation to a friend">
                <livewire:create-invitation />
            </x-profile-card>            

            <x-profile-card title="Pending invitations" subtitle="Lorem ipsum">
                <livewire:invitation-table status='pending' />
            </x-profile-card>

            <x-profile-card title="Accepted invitations" subtitle="Lorem ipsum">
                <livewire:invitation-table status="accepted"/>
            </x-profile-card>

        </div>
    </div>
</x-app-layout>