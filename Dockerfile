FROM php:apache

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG PROJECT_PATH
RUN : "${PROJECT_PATH:=$PWD}"

RUN export DEBIAN_FRONTEND=noninteractive       \
  && apt-get update                             \
  && apt-get install -y --no-install-recommends \
        git                                     \
        libzip-dev                              \
        unzip                                   \
        zip                                     \
        libpq-dev                               \
  && rm -rf /var/lib/apt/lists/*                \
  && a2enmod rewrite                            \
  && a2enmod ssl                                \
  && docker-php-ext-install zip                 \
  && docker-php-ext-install pgsql               \
  && docker-php-ext-install pdo_pgsql           \
  && curl -fsSL https://deb.nodesource.com/setup_21.x | bash - \
  && apt-get install -y nodejs

#WORKDIR /tls
#RUN openssl req       \
#  -days 365           \
#  -keyout server.key  \
#  -new                \
#  -nodes              \
#  -out server.cert    \
#  -subj "/C=NL/ST=Overijssel/L=Enschede/O=PDS Interop/OU=IT/CN=pdsinterop.org" \
#  -x509

WORKDIR /var/www/html
COPY site.conf /etc/apache2/sites-enabled/000-default.conf

#COPY "${PROJECT_PATH}/composer.json" /var/www/html/composer.json
COPY "${PROJECT_PATH}/" /var/www/html
RUN composer install --no-dev --prefer-dist

#COPY "${PROJECT_PATH}/app/src/" /var/www/html/src


EXPOSE 80