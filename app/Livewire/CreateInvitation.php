<?php

namespace App\Livewire;

use App\Models\Invitation;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;
use Livewire\Component;

class CreateInvitation extends Component
{
    #[Validate(['required', 'string', 'max:255'])]
    public $name;

    #[Validate(['required', 'string', 'lowercase', 'email', 'max:255', 'unique:'.Invitation::class])]
    public $email;

    #[Validate(['required', 'string'])]
    public $message;

    public function save()
    {
        $this->validate(); 
        
        $user = Auth::user();

        $invitation = $user->invitations()->create(
            $this->only(['name', 'email', 'message'])
        );

        $invitation->send();

 
        return $this->redirect('/invitation');
    }

    public function render()
    {
        return view('livewire.create-invitation');
    }
}
