<?php

namespace App\Livewire;

use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Invitation;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Columns\ButtonGroupColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\LinkColumn;

class InvitationTable extends DataTableComponent
{
    #protected $model = Invitation::class;
    public string $status;

    public function builder(): Builder
    {
        #dd($this->status);
        if ($this->status == 'accepted') {
            return Invitation::where('accepted_at', '!=', null);
        }
        if ($this->status == 'pending') {
            return Invitation::where('accepted_at', null);
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setSearchDisabled();
        $this->setPerPage(10);
        $this->setPerPageVisibilityDisabled();
        $this->setColumnSelectDisabled();
    }

    

    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->sortable(),
            Column::make("Name", "name")
                ->sortable(),
            Column::make("Email", "email")
                ->sortable(),
            Column::make("Created at", "created_at")
                ->sortable(),
            Column::make('Action')
                ->label(function ($row, Column $column) {
                    $links = ['deleteLink' => route('delete.invitation', $row->id)];
                    if ($this->status == 'accepted') {
                        $links['customLink'] = [
                            'location' => '/resend',
                            'method' => 'POST',
                            'confirmMessage' => 'Resend invitation?',
                            'label' => 'Resend invitation'
                        ];
                    }
                    return view(
                        'components.livewire.datatables.action-column',
                        $links
                    );
                })
                ->html() 
        ];
    }
}
