<?php

namespace App\Http\Controllers;

use App\Models\Invitation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class InvitationController extends Controller
{

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:'.Invitation::class],
            'message' => ['required', 'string']
        ]);

        $user = Auth::user();
        $invitation = $user->invitations()->create([
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message
        ]);

        $invitation->send();

        return response()->json($invitation);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        return $this->deleteInvitation($request);
    }

     /**
     * Remove the specified resource from storage.
     */
    public function delete(Request $request)
    {
        if (!$this->deleteInvitation($request)) {

        }
        return redirect()->back()->with('success', 'Invitation deleted successfully');
    }

    private function deleteInvitation(Request $request) {
        $invitation = Invitation::findOrFail($request->id);
        $invitation->delete();
    }
}
