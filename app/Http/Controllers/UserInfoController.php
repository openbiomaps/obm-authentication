<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserInfoController extends Controller
{
    public function getUserInfo(Request $request)
    {
        $user = Auth::user();

        $userInfo = [
            'email' => $user->email,
            'name' => $user->name
        ];

        return response()->json($userInfo);
    }
}
