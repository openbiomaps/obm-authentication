<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IsRegistrationOpen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->path() !== 'accept-invitation' && config('app.user_registration') == 'invite') {
            return redirect('accept-invitation');
        }

        if ($request->path() !== 'registration-disabled' && config('app.user_registration') == 'disabled') {
            return redirect('registration-disabled');
        }

        if ($request->path() !== 'register' && config('app.user_registration') == 'open') {
            return redirect('register');
        }

        return $next($request);
    }
}
