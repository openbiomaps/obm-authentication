<?php

namespace App\Models;

use App\Mail\InvitationMail;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;

class Invitation extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'email', 'message'];

    protected static function booted()
    {
        parent::booted();

        static::creating(function ($model) {
            // Check if the attribute is not set, then set the default value using a function
            if (empty($model->code)) {
                $model->code = Uuid::uuid4();
            }
        });
    }

    public function send() {
        $inv_message = $this->message;
        
        Mail::to($this->email)->send(new InvitationMail(
            $this
        ));
    }

}
