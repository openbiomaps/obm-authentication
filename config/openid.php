<?php

return [

    /**
     * Optional associative array that will be used to set headers on the JWT
     */
    'token_headers' => [
        'kid' => base64_encode('public-key-added-2023-01-01')
    ],

    /**
     * By default, microseconds are included.
     */
    'use_microseconds' => false,
];